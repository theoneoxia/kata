import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Issue } from './app/interfaces/issue.interface';

@Injectable({
  providedIn: 'root'
})
export class GithubService {
  baseUrl = 'https://api.github.com';

  constructor(private http: HttpClient) { }

  getIssues() {
    return this.http.get<Issue[]>(`${this.baseUrl}/repos/microsoft/typescript/issues?sort=created&direction=desc&per_page=5`);
  }

  getRepositoriesForUser(userUrl: string){
    return this.http.get<any[]>(`${userUrl}/repos?sort=created&direction=desc&per_page=3`);
  }
}
