import { IGithubUser } from './githubUser.interface';

export interface Issue {
    title: string;
    state: string;
    user: IGithubUser;
}