export interface IGithubUser {
    avatar_url: string;
    url: string;
}