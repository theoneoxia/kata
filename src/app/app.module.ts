import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component';
import { GithubService } from 'src/github.service';

import { HttpClientModule } from '@angular/common/http';
import { IssueComponent } from './issues/issues.component';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    IssueComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [GithubService],
  bootstrap: [AppComponent]
})
export class AppModule { }
