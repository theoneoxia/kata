import { Component, OnInit, Input } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { GithubService } from 'src/github.service';
import { Issue } from '../interfaces/issue.interface';

@Component({
  selector: 'app-issue',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.scss']
})
export class IssueComponent {
    @Input() issue: Issue;
}
