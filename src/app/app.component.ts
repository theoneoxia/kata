import { Component, OnInit } from '@angular/core';
import { GithubService } from 'src/github.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'kata1';
}
