import { Component, OnInit, Input } from '@angular/core';
import { Observable, Subject,  } from 'rxjs';
import { GithubService } from 'src/github.service';
import { Issue } from '../interfaces/issue.interface';
import { mergeMap} from 'rxjs/operators';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  $issues: Subject<Issue[]> = new Subject<Issue[]>();
  issues: Issue[] =  [];

  constructor(private readonly gitHubService: GithubService) {
  }

  ngOnInit() {
   this.getDatas();

   this.$issues.subscribe(issues => {
      console.log(issues);
      this.issues = issues;
   });
  }

  async getDatas() {
    this.gitHubService.getIssues()
    .pipe(mergeMap(issues => issues.map(async issue => {
      const repositories = await this.gitHubService.getRepositoriesForUser(issue.user.url).toPromise();
      return repositories;
    })))
    .subscribe(data => {
      console.log(data);
   });
  }

}


